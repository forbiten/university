﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Authorization
{
    public class RoleConstants
    {
        /// <summary>
        /// Администратор
        /// </summary>
        [DefineRole]
        public const string RoleAdministrator = "Administrator";

        /// <summary>
        /// Модератор
        /// </summary>
        [DefineRole]
        public const string RoleModerator = "Moderator";
    }
}
