﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace University.Authorization
{
    public static class PolicyServiceBuilderExtensions
    {
        public static IServiceCollection AddConstantAuthorizationPolicy(this IServiceCollection services)
        {
            if(services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddAuthorization(options =>
            {
                var definitions = AuthorizationHelper.GetPolicyDefines<PolicyConstants>();
                foreach (var definition in definitions)
                {
                    options.AddPolicy(definition.Value, policy => definition.Attribute.Configure(policy));
                }
            });

            return services;
        }
    }
}
