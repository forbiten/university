﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.Authorization
{
    public static class RoleServiceBuilderExtensions
    {
        /// <summary>
        /// Создает все роли определенные в RoleDefaults
        /// </summary>
        /// <param name="roleManager"></param>
        public static async Task<IServiceProvider> AddConstantRoles<TContext>(this IServiceProvider services)
            where TContext : DbContext
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            await CreateConstantRoles(services);
            return services;
        }

        private static async Task CreateConstantRoles(IServiceProvider services)
        {
            using (var scope = services.CreateScope())
            {
                var scopedProvider = scope.ServiceProvider;
                var roleManager = scopedProvider.GetService<RoleManager<IdentityRole>>();
                var roles = AuthorizationHelper.GetIdentityRoleDefines<RoleConstants>();

                foreach (var role in roles)
                {
                    /// Проверяем, есть ли роль
                    if (await roleManager.RoleExistsAsync(role.Name)) continue;

                    /// Создаем роль
                    var result = await roleManager.CreateAsync(role);
                }
            }
                
        }
    }
}
