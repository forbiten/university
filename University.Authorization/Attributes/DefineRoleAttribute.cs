﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Authorization
{
    [AttributeUsage(AttributeTargets.Field)]
    public class DefineRoleAttribute : Attribute
    {
    }
}
