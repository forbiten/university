﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Authorization
{
    public class PolicyConstants
    {
        /// <summary>
        /// Менеджеры. Администратор и модератор.
        /// </summary>
        [PolicyRequirements(Roles = new string[] { RoleConstants.RoleAdministrator, RoleConstants.RoleModerator })]
        public const string PolicyManagers = "Managers";
    }
}
