﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using University.Communication;
using University.Database.Models;
using University.ViewModels;
using System.Security.Claims;

namespace University.Controllers
{
    /// <summary>
    /// Котроллер паспорта.
    /// Здесь находятся страницы и методы для управления аутентификацией, регистрацией, восстановления доступа и т.д.
    /// </summary>
    [Authorize]
    [Route("[controller]/[action]")]
    public class PassportController : Controller
    {
        private readonly UserManager<User> m_UserManager;
        private readonly SignInManager<User> m_SignInManager;
        private readonly ILogger m_Logger;
        private readonly IEmailSender m_EmailSender;

        private Dictionary<string, ErrorModelRoute> m_ErrorModelRoutes = new Dictionary<string, ErrorModelRoute>
        {
            { "DuplicateEmail", new ErrorModelRoute { key = "Email", message = "Такой email уже зарегистрирован" } },
            { "InvalidEmail", new ErrorModelRoute { key = "Email", message = "Неверный email" } }
        };

        private struct ErrorModelRoute
        {
            public string key;
            public string message;
        }

        public PassportController(
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            IEmailSender emailSender,
            ILogger<PassportController> logger)
        {
            m_UserManager = userManager;
            m_SignInManager = signInManager;
            m_EmailSender = emailSender;
            m_Logger = logger;
        }

        /// Страницы авторизации.
        /// Методы представляющие механизмы авторизации в системе.
        /// За исключением внутренней системы авторизации 
        /// так-же здесь находятся механизмы для авторизации через стороннии сервися
        #region Login methods

        /// <summary>
        /// Страница входа в систему.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Очищаем существующие внешние куки для валидного процесса входа
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            WriteReturnUrl(returnUrl);
            return View();
        }

        /// <summary>
        /// Метод процесса авторизации на сайте.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(PassportLoginViewModel model, string returnUrl = null)
        {
            WriteReturnUrl(returnUrl);
            if (ModelState.IsValid)
            {
                var user = await m_UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    ModelState.AddModelError(string.Empty, "Логин или пароль неверны");
                    return View(model);
                }

                var result = await m_SignInManager.PasswordSignInAsync(user, model.Password, true, lockoutOnFailure: true);

                if (result.Succeeded)
                {
                    m_Logger.LogInformation("User logged in.");
                    return RedirectToLocal(returnUrl);
                }

                if (result.IsLockedOut)
                {
                    m_Logger.LogWarning("User account locked out.");
                    return RedirectToAction(nameof(Lockout));
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Логин или пароль неверны");
                    return View(model);
                }
            }

            // Что-то пошло не так, возвращаем страницу авторизации
            return View(model);
        }

        #endregion

        /// Страницы аутентификации через внешние источники.
        #region External authentication pages

        /// <summary>
        /// Метод запрос аутентификации у стороннего ресурса
        /// </summary>
        /// <param name="provider"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Запрос редиректа на стороннюю аутентификацию.
            var redirectUrl = Url.Action(nameof(ExternalLoginCallback), GetControllerRouteName(), new { returnUrl });
            /// var redirectUrl = "/passport/oauth2callback";
            var properties = m_SignInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        [HttpGet]
        [AllowAnonymous]
        ///[Route("/signin-google")]
        ///[Route("/signin-vkontakte")]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                return RedirectToLogin();
            }

            var info = await m_SignInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToLogin();
            }

            // Вход для зарегистрированного пользователя по внешним параметрам входа
            var result = await m_SignInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            if (result.Succeeded)
            {
                m_Logger.LogInformation($"User logged in with {info.LoginProvider} provider.");
                return RedirectToLocal(returnUrl);
            }

            if (result.IsLockedOut)
            {
                return RedirectToLockout();
            }
            else
            {
                // Если у пользователя нет аккаунта, то спрашиваем его о создании аккаунта на основе внешних данных.
                ViewData["ReturnUrl"] = returnUrl;
                ViewData["LoginProvider"] = info.LoginProvider;
                
                return View("ExternalLogin", new PassportExternalLoginViewModel
                {
                    Email = info.Principal.FindFirstValue(ClaimTypes.Email),
                    FirstName = info.Principal.FindFirstValue(ClaimTypes.GivenName),
                    LastName = info.Principal.FindFirstValue(ClaimTypes.Surname)
                });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(PassportExternalLoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Получение информации о пользователи по данным из внешнего источника.
                var info = await m_SignInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    throw new ApplicationException("Error loading external login information during confirmation.");
                }

                var user = Database.Models.User.CreateNewUser();
                user.Email = model.Email;
                user.UserFirstName = model.FirstName;
                user.UserMiddleName = model.MiddleName;
                user.UserLastName = model.LastName;
                
                var result = await m_UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await m_UserManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await m_SignInManager.SignInAsync(user, isPersistent: true);
                        m_Logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return await SendEmailConfirmationAndRedirectToEmailConfirmation(user);
                    }
                }

                AddErrors(result);
            }

            ViewData["ReturnUrl"] = returnUrl;
            return View(nameof(ExternalLogin), model);
        }

        #endregion

        /// Страницы регистрации.
        /// Методы представляющие механизмы регистрации в системе.
        #region Registration

        /// <summary>
        /// Страница регистрации нового аккаунта.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            WriteReturnUrl(returnUrl);
            return View();
        }

        /// <summary>
        /// Метод процесса регистрации нового аккаунта
        /// </summary>
        /// <param name="model"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(PassportRegisterViewModel model, string returnUrl = null)
        {
            WriteReturnUrl(returnUrl);
            if (ModelState.IsValid)
            {
                var user = Database.Models.User.CreateNewUser();
                user.Email = model.Email;
                user.UserFirstName = model.FirstName;
                user.UserMiddleName = model.MiddleName;
                user.UserLastName = model.LastName;

                var result = await m_UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    m_Logger.LogInformation("User created a new account with password.");
                    return await SendEmailConfirmationAndRedirectToEmailConfirmation(user);
                }

                AddErrors("Password", result);
            }

            // Что-то пошло не так, пересоздаем форму
            return View(model);
        }

        #endregion

        /// Страницы восстановления пароля доступа.
        #region Reset password

        /// <summary>
        /// Страница восстановления пароля
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(string userId = null, string code = null)
        {
            if (userId == null || code == null)
            {
                return RedirectToLocal(null);
            }

            var user = await m_UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                return RedirectToLocal(null);
            }

            var model = new PassportResetPasswordViewModel { Code = code, Email = user.Email };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(PassportResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            /// Стандартное положительное сообщение
            ViewData["ResetPasswordConfirmationMessage"] = "Пароль был успешно изменен.";

            var user = await m_UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Не показываем, что пользователя не существует
                return View("ResetPasswordConfirmation");
            }
            
            var result = await m_UserManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return View("ResetPasswordConfirmation");
            }
            else
            {
                /// Токен не прошел валидацию
                ViewData["ResetPasswordConfirmationMessage"] = "Упс, что-то пошло не так. Попробуйте запросить ссылку на воостановление пароля еще раз.";
                return View("ResetPasswordConfirmation");
            }

            /*
            AddErrors(result);
            return View(model);
            */
        }

        #endregion

        /// Страницы запроса восстановления пароля доступа.
        #region Forgot password

        /// <summary>
        /// Страница восстановления доступа к аккаунту.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword(string email = null)
        {
            return View(new PassportForgotViewModel { Email = email });
        }

        /// <summary>
        /// Метод воостановления пароля доступа к системе
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(PassportForgotViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await m_UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Не показываем, что почта не зарегистрирована
                    return View("ForgotPasswordConfirmation");
                }

                var emailConfirmed = await m_UserManager.IsEmailConfirmedAsync(user);
                if (!emailConfirmed)
                {
                    return await SendEmailConfirmationAndRedirectToEmailConfirmation(user,
                        @"Похоже, что вы еще не подтвердили свою почту,
                      прежде, чем вы сможете сменить пароль, вам необходимо подтвердить вашу почту.
                      Мы повторно отправили вам уведомление с ссылкой на подтверждение почты.");
                }

                await SendPasswordResetLinkAsync(user);
                return View("ForgotPasswordConfirmation");
            }

            // Что-то пошло не так, пересоздаем форму
            return View(model);
        }

        #endregion

        /// Страницы обработки подтверждения email.
        #region Email confirmation

        /// <summary>
        /// Страница подтверждения почты.
        /// Непосредственно подтверждает почту.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> EmailConfirmation(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToLocal(null);
            }

            var isSuccess = false;

            var user = await m_UserManager.FindByIdAsync(userId);
            if (user != null)
            {
                var confirmed = await m_UserManager.IsEmailConfirmedAsync(user);

                /// Почта не нуждается в подтверждении
                /// Следовательно запрос на подтверждение невалиден
                if (!confirmed)
                {
                    var result = await m_UserManager.ConfirmEmailAsync(user, code);
                    isSuccess = result.Succeeded;
                }
            }
            ViewData["EmailConfirmationMessage"] = isSuccess 
                ? "Ваш email был успешно подтвержден"
                : "Ошибка подтверждения email. Ваш код подтверждения истек или невалиден.";
            return View("EmailConfirmation");
        }

        #endregion

        /// Вспомогательные страницы.
        /// Например: выход из системы, страницы блокировки и т.д.
        #region Utils

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await m_SignInManager.SignOutAsync();
            m_Logger.LogInformation("User logged out.");

            return RedirectToLocal(null);
        }
        
        /// <summary>
        /// Страница временной блокировки доступа к аккаунту.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return View();
        }

        #endregion

        /// Вспомогательные методы.
        #region Helpers

        private async Task<IActionResult> SendEmailConfirmationAndRedirectToEmailConfirmation(User user)
        {
            return await SendEmailConfirmationAndRedirectToEmailConfirmation(user, "На ваш email было отправлено уведомление с ссылкой на подтверждение email. Также не забудьте проверить спам.");
        }

        private async Task<IActionResult> SendEmailConfirmationAndRedirectToEmailConfirmation(User user, string showMessage)
        {
            await SendEmailConfirmationLinkAsync(user);

            ViewData["EmailConfirmationMessage"] = showMessage;
            return View("EmailConfirmation");
        }

        /// <summary>
        /// Отправляет пользователю уведомление с ссылкой на подтверждение почты.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task SendEmailConfirmationLinkAsync(User user)
        {
            try
            {
                var code = await m_UserManager.GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = EmailConfirmationLink(user.Id, code, Request.Scheme);

                await m_EmailSender.SendEmailConfirmationAsync(user.Email, callbackUrl);
            }
            catch (Exception exception)
            {
                m_Logger.LogWarning($"Send email confirmation link failed. Exception: {exception}");
            }
        }

        /// <summary>
        /// Отправляет пользователю уведомление с ссылкой на смену пароля для входа.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        private async Task SendPasswordResetLinkAsync(User user)
        {
            try
            {
                var code = await m_UserManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = ResetPasswordCallbackLink(user.Id, code, Request.Scheme);

                await m_EmailSender.SendPasswordResetAsync(user.Email, callbackUrl);
            }
            catch (Exception exception)
            {
                m_Logger.LogWarning($"Send password reset link failed. Exception: {exception}");
            }
        }

        /// <summary>
        /// Добавялет ошибки из IdentityResult в модель
        /// </summary>
        /// <param name="result"></param>
        private void AddErrors(IdentityResult result)
        {
            AddErrors(string.Empty, result);
        }

        /// <summary>
        /// Добавялет ошибки из IdentityResult в модель по ключу
        /// </summary>
        /// <param name="result"></param>
        private void AddErrors(string channel, IdentityResult result)
        {
            if (result == null) return;

            foreach (var error in result.Errors)
            {
                var routeModel = TryGetErrorModelRoute(error.Code);
                m_Logger.LogInformation($"Code: {error.Code}, Description: {error.Description}");
                
                if (routeModel != null && routeModel.HasValue)
                {
                    ModelState.AddModelError(routeModel.Value.key, routeModel.Value.message);
                }
                else
                {
                    ModelState.AddModelError(channel, error.Description);
                }
            }
        }

        /// <summary>
        /// Записывает адрес для возвращения в ViewData
        /// </summary>
        /// <param name="url"></param>
        private void WriteReturnUrl(string url)
        {
            ViewData["ReturnUrl"] = url;
        }

        /// <summary>
        /// Редирект на указунную локальную страницу или домашнюю
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToHome();
            }
        }

        /// <summary>
        /// Редирект на домашнюю страницу.
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private IActionResult RedirectToHome()
        {
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        /// <summary>
        /// Редирект на страницу регистрации.
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private IActionResult RedirectToLogin()
        {
            return RedirectToAction(nameof(PassportController.Login), GetControllerRouteName());
        }

        /// <summary>
        /// Редирект на страницу блокировки.
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        private IActionResult RedirectToLockout()
        {
            return RedirectToAction(nameof(PassportController.Lockout), GetControllerRouteName());
        }

        private string EmailConfirmationLink(string userId, string code, string scheme)
        {
            return Url.Action(
                action: nameof(EmailConfirmation),
                controller: GetControllerRouteName(),
                values: new { userId, code },
                protocol: scheme);
        }

        private string ResetPasswordCallbackLink(string userId, string code, string scheme)
        {
            return Url.Action(
                action: nameof(ResetPassword),
                controller: GetControllerRouteName(),
                values: new { userId, code },
                protocol: scheme);
        }

        private string GetControllerRouteName()
        {
            return ControllerContext.RouteData.Values["controller"].ToString();
        }

        private ErrorModelRoute? TryGetErrorModelRoute(string code)
        {
            var hasRoute = m_ErrorModelRoutes.ContainsKey(code);
            return hasRoute ? m_ErrorModelRoutes[code] : (ErrorModelRoute?)null;
        }

        #endregion
    }
}
