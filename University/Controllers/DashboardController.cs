﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using University.Api;
using University.Authorization;
using University.Database;
using University.ViewModels;

namespace University.Controllers
{
    [Authorize(Policy = PolicyConstants.PolicyManagers)]
    [Route("[controller]/[action]")]
    public class DashboardController : Controller
    {
        private readonly ILogger m_Logger;
        private readonly EducationSpecialtyManager m_EducationSpecialtyManager;
        private readonly EducationMethodManager m_EducationMethodManager;
        private readonly InstructorManager m_InstructorManager;
        private readonly DisciplineManager m_DisciplineManager;
        private readonly DisciplineMethodManager m_DisciplineMethodManager;
        private readonly ScheduleManager m_ScheduleManager;
        private readonly ScheduleRecordManager m_ScheduleRecordManager;
        private readonly ScheduleSheetManager m_ScheduleSheetManager;
        private readonly ScheduleTypeManager m_ScheduleTypeManager;
        private readonly StudentGroupManager m_StudentGroupManager;
        private readonly QualificationManager m_QualificationManager;

        public DashboardController(
            ILogger<DashboardController> logger,
            EducationSpecialtyManager educationSpecialtyManager,
            EducationMethodManager educationMethodManager,
            InstructorManager instructorManager,
            DisciplineManager disciplineManager,
            DisciplineMethodManager disciplineMethodManager,
            ScheduleManager scheduleManager,
            ScheduleRecordManager scheduleRecordManager,
            ScheduleSheetManager scheduleSheetManager,
            ScheduleTypeManager scheduleTypeManager,
            StudentGroupManager studentGroupManager,
            QualificationManager qualificationManager)
        {
            m_Logger = logger;
            m_EducationSpecialtyManager = educationSpecialtyManager;
            m_EducationMethodManager = educationMethodManager;
            m_InstructorManager = instructorManager;
            m_DisciplineManager = disciplineManager;
            m_DisciplineMethodManager = disciplineMethodManager;
            m_ScheduleManager = scheduleManager;
            m_ScheduleRecordManager = scheduleRecordManager;
            m_ScheduleSheetManager = scheduleSheetManager;
            m_ScheduleTypeManager = scheduleTypeManager;
            m_StudentGroupManager = studentGroupManager;
            m_QualificationManager = qualificationManager;
        }

        [HttpGet] [Route("")] [Route("/[controller]/")]
        public IActionResult Index() => View();

        #region Education specialties
        [HttpGet] [Route("/[controller]/specialties")]
        public IActionResult EducationSpecialtiesManager() => View();

        [HttpPost] [Route("/[controller]/specialties")]
        public IActionResult EducationSpecialtiesManagerPartial() => View();

        [HttpPost] [Route("/[controller]/specialties/forms.edit")]
        public IActionResult EducationSpecialtiesEditForm() => View();
        #endregion

        #region Education methods
        [HttpGet] [Route("/[controller]/education/methods")]
        public IActionResult EducationMethodsManager() => View();

        [HttpPost] [Route("/[controller]/education/methods")]
        public IActionResult EducationMethodsManagerPartial() => View();

        [HttpPost] [Route("/[controller]/education/methods/forms.edit")]
        public IActionResult EducationMethodsEditForm() => View();
        #endregion

        #region Instructors
        [HttpGet] [Route("/[controller]/instructors")]
        public IActionResult InstructorsManager() => View();

        [HttpPost] [Route("/[controller]/instructors")]
        public IActionResult InstructorsManagerPartial() => View();

        [HttpPost] [Route("/[controller]/instructors/forms.edit")]
        public IActionResult InstructorsEditForm() => View();
        #endregion

        #region Disciplines
        [HttpGet] [Route("/[controller]/disciplines")]
        public IActionResult DisciplinesManager() => View();

        [HttpPost] [Route("/[controller]/disciplines")]
        public IActionResult DisciplinesManagerPartial() => View();

        [HttpPost] [Route("/[controller]/disciplines/forms.edit")]
        public IActionResult DisciplinesEditForm() => View();
        #endregion

        #region Discipline methods
        [HttpGet] [Route("/[controller]/disciplineMethods")]
        public IActionResult DisciplineMethodsManager() => View();

        [HttpPost] [Route("/[controller]/disciplineMethods")]
        public IActionResult DisciplineMethodsManagerPartial() => View();

        [HttpPost] [Route("/[controller]/disciplineMethods/forms.edit")]
        public IActionResult DisciplineMethodsEditForm() => View();
        #endregion
        
        #region Qualifications
        [HttpGet] [Route("/[controller]/education/qualifications")]
        public IActionResult QualificationsManager() => View();

        [HttpPost] [Route("/[controller]/education/qualifications")]
        public IActionResult QualificationsManagerPartial() => View();

        [HttpPost] [Route("/[controller]/education/qualifications/forms.edit")]
        public IActionResult QualificationsEditForm() => View();
        #endregion
        
        #region Student groups
        [HttpGet] [Route("/[controller]/students/groups")]
        public async Task<IActionResult> StudentGroupsManager() => View(await GetDashboardStudentGroupManagerDisplayViewModel());

        [HttpPost] [Route("/[controller]/students/groups")]
        public async Task<IActionResult> StudentGroupsManagerPartial() => View(await GetDashboardStudentGroupManagerDisplayViewModel());

        [HttpPost] [Route("/[controller]/students/groups/forms.edit")]
        public async Task<IActionResult> StudentGroupsEditForm() => View(await GetDashboardStudentGroupManagerDisplayViewModel());
        #endregion

        #region Schedules home
        [HttpGet] [Route("/[controller]/schedules")]
        public async Task<IActionResult> SchedulesHome() => View(await GetDashboardScheduleManagerDisplayViewModel());

        [HttpPost] [Route("/[controller]/schedules")]
        public async Task<IActionResult> SchedulesHomePartial() => View(await GetDashboardScheduleManagerDisplayViewModel());
        #endregion

        #region Schedules manager
        [HttpGet] [Route("/[controller]/schedules/editor")]
        public Task<IActionResult> SchedulesEditor(DashboardScheduleEditRequestViewModel model) => MakeScheduleEditorRequestHandler(model);

        [HttpPost] [Route("/[controller]/schedules/editor")]
        public Task<IActionResult> SchedulesEditorPartial(DashboardScheduleEditRequestViewModel model) => MakeScheduleEditorRequestHandler(model);
        #endregion

        #region Schedule types
        [HttpGet] [Route("/[controller]/schedules/types")]
        public IActionResult ScheduleTypesManager() => View();

        [HttpPost] [Route("/[controller]/schedules/types")]
        public IActionResult ScheduleTypesManagerPartial() => View();

        [HttpPost] [Route("/[controller]/schedules/types/forms.edit")]
        public IActionResult ScheduleTypesEditForm() => View();
        #endregion


        private async Task<IActionResult> MakeScheduleEditorRequestHandler(DashboardScheduleEditRequestViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var schedule = (await m_ScheduleManager.FindByIdAsync(model.ScheduleId)).Entity;
            if (schedule == null)
                return BadRequest();

            var displayModel = new DashboardScheduleEditorDisplayViewModel(
                schedule,
                await m_DisciplineManager.Repository.GetAsync(),
                await m_DisciplineMethodManager.Repository.GetAsync(),
                await m_InstructorManager.Repository.GetAsync());
            return View(displayModel);
        }

        private IActionResult MakeScheduleEditorScheduleNotFoundRequest(DashboardScheduleEditRequestViewModel model)
        {
            return new JsonResult(new { error = $"Schedule by id: '{model.ScheduleId}' not exists" });
        }

        private async Task<DashboardScheduleManagerDisplayViewModel> GetDashboardScheduleManagerDisplayViewModel()
        {
            return new DashboardScheduleManagerDisplayViewModel(
                await m_StudentGroupManager.Repository.GetAsync(),
                await m_ScheduleTypeManager.Repository.GetAsync());
        }

        private async Task<DashboardStudentGroupManagerDisplayViewModel> GetDashboardStudentGroupManagerDisplayViewModel()
        {
            return new DashboardStudentGroupManagerDisplayViewModel(
                await m_QualificationManager.Repository.GetAsync(),
                await m_EducationSpecialtyManager.Repository.GetAsync(),
                await m_EducationMethodManager.Repository.GetAsync());
        }
    }
}