﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace University.Controllers
{
    using ViewModels;

    [Route("[controller]/[action]")]
    public class SecurityController : Controller
    {
        public SecurityController()
        {
        }
        
        public IActionResult Denied()
        {
            return View();
        }
    }
}
