﻿function ScheduleEditorSetupOptions(sheduleId, rootElement)
{
    this.scheduleId = sheduleId;
    this.rootElement = rootElement;

    this.sheetItemTemplate = "";
    this.recordListTemplate = "";
    this.recordItemTemplate = "";
}

window.dashboard.scheduleEditor = (function ()
{
    var creatorRequestMode = {
        create: "create",
        edit: "edit"
    };
    return new ScheduleEditor();

    function ScheduleEditor()
    {
        var scheduleId = "";
        var that = this;
        var scheduleShortData;
        var schedulePublicationState = 0;

        var scheduleSheetsManager,
            scheduleRecordsManager;

        var elementReferences = {};
        var controlReferences = {};

        var publicateButton,
            publicationState;

        var blocker;

        this.setup = function (options)
        {
            removeAllManagersEventListeners();
            removeAllManagers();

            scheduleId = options.scheduleId;
            publicationState = options.rootElement.querySelector("[data-ref='schedule-publication-state']");
            publicateButton = options.rootElement.querySelector("[control='schedule-publication']");
            publicateButton.addEventListener("click", function ()
            {
                if (scheduleShortData == undefined || scheduleShortData == null) return;
                that.setPublicationStateCode(schedulePublicationState == 0 ? 1 : 0);
            });

            var blockerElements = options.rootElement.querySelectorAll("[blocker]");
            blocker = new Blocker(blockerElements[blockerElements.length - 1]);
            
            /// Создаем менеджер листов
            scheduleSheetsManager = new ScheduleSheetManager(
                options.scheduleId,
                window.api.catalog.scheduleSheets,
                options.rootElement.querySelector("[data-ref='schedule-sheets-manager']"),
                options.sheetItemTemplate,
                blocker);

            /// Создаем менеджер записей
            scheduleRecordsManager = new ScheduleRecordManager(
                options.scheduleId,
                options.rootElement.querySelector("[data-ref='schedule-records-manager']"),
                options.recordListTemplate,
                options.recordItemTemplate,
                blocker);

            addAllManagersEventListeners();

            that.updateScheduleDetails();
        }

        this.update = function ()
        {
            scheduleSheetsManager.forceUpdate();
        };

        this.updateScheduleDetails = function ()
        {
            api.catalog.schedules.get({ Id: scheduleId }, (response) =>
            {
                if (response["response"])
                {
                    scheduleShortData = response["response"];
                    schedulePublicationState = scheduleShortData.publicationStatus;
                    updatePublicationStateContent(schedulePublicationState);
                }
                else
                {
                    console.error(response);
                }
            });
        };

        this.setPublicationStateCode = function (state)
        {
            blocker.setActive(true);
            api.catalog.schedules.publication({ ScheduleId: scheduleId, State: state }, (response) =>
            {
                blocker.setActive(false);
                if (response["response"])
                {
                    schedulePublicationState = response["response"].publicationStatus;
                    updatePublicationStateContent(schedulePublicationState);
                }
                else
                {
                    console.error(response);
                }
            });
        };

        function updatePublicationStateContent(status)
        {
            publicateButton.innerHTML = status ? "Убрать публикацию" : "Опубликовать";
            if (status) publicateButton.classList.add("published");
            else publicateButton.classList.remove("published");

            publicationState.innerHTML = status ? "Расписание опубликовано" : "Расписание не опубликовано";
        }

        function onSheetSelected(sheet)
        {
            scheduleRecordsManager.setSheet(sheet);
        }

        function removeAllManagers()
        {
            scheduleSheetsManager = null;
            scheduleRecordsManager = null;
        }

        function addAllManagersEventListeners()
        {
            addSheetManagerEventListeners();
        }

        function removeAllManagersEventListeners()
        {
            removeSheetManagerEventListeners();
        }

        function addSheetManagerEventListeners()
        {
            if (scheduleSheetsManager == null) return;
            scheduleSheetsManager.onSheetSelectionChanged.addEventListener(onSheetSelected);
        }

        function removeSheetManagerEventListeners()
        {
            if (scheduleSheetsManager == null) return;
            scheduleSheetsManager.onSheetSelectionChanged.removeEventListener(onSheetSelected);
        }
    }

    function ScheduleSheetManager(scheduleId, apiCatalog, rootElement, itemTemplate, rootBlocker)
    {
        var that = this;
        var elementsReferences = new ReferenceElementsBuilder("data-ref").build(rootElement);
        var controlsReferences = new ReferenceElementsBuilder("control").build(rootElement);
        
        var listElement = elementsReferences["schedule-sheets-list"].element,
            addSheetButton = controlsReferences["add"].element;

        var blocker = new Blocker(rootElement.querySelector("[blocker]"));

        var items = [];
        var loadedData = [];
        
        var selectedSheet = undefined;

        this.getAllSheets = function ()
        {
            return loadedData;
        };

        this.onSheetSelectionChanged = new EventHandler();

        this.selectSheetByIndex = function (index)
        {
            try
            {
                setSelectedSheet(loadedData[index]);
            }
            catch (err)
            {
                console.error(err);
            }
        };

        this.selectSheetById = function (id)
        {
            try
            {
                for (var i = 0; i < loadedData.length; i++)
                {
                    if (loadedData[i].id == id)
                    {
                        setSelectedSheet(loadedData[i]);
                        return;
                    }
                }
            }
            catch (err)
            {
                console.error(err);
            }
        };

        this.getSelectedSheet = function ()
        {
            return selectedSheet;
        };

        this.removeItems = function ()
        {
            for (var i = 0; i < items.length; i++)
                items[i].remove();
            items = [];
        };

        this.forceUpdate = function (callback)
        {
            callback = utils.normalizeFunction(callback);

            blocker.setActive(true);
            rootBlocker.setActive(true);
            apiCatalog.getLinked({ ScheduleId: scheduleId }, (response) =>
            {
                if (response["response"])
                {
                    loadedData = response["response"];
                    updateLoadedData();
                    
                    that.update();
                    setSelectedSheet(loadedData[0]);
                }
                else
                {
                    console.error(response);
                }

                blocker.setActive(false);
                rootBlocker.setActive(false);
                callback();
            });
        };

        this.update = function ()
        {
            var oldLoadedData = [].slice.call(loadedData);

            that.removeItems();

            loadedData = oldLoadedData;
            updateLoadedData();

            if (loadedData.length <= 0) return;

            var maxWeekdayIndex = loadedData.reduce((max, p) => p.weekPriorityIndex > max ? p.weekPriorityIndex : max, loadedData[0].weekPriorityIndex);
            for (var i = 0; i < loadedData.length; i++)
            {
                var item = new ScheduleSheetItem(loadedData[i], that, maxWeekdayIndex, apiCatalog, listElement, itemTemplate);
                item.onRemove.addEventListener(onItemRemoved);
                item.onSelect.addEventListener(onItemSelected);

                items.push(item);
            }
        };

        function onSelectedSheetChanged()
        {
            var item;
            for (var i = 0; i < items.length; i++)
            {
                item = items[i];
                if (item == null) continue;

                item.setSelectedStatus(item.getData() == selectedSheet);
            }

            that.onSheetSelectionChanged.invoke(selectedSheet);
        }

        function setSelectedSheet(data)
        {
            if (data == null) return;

            selectedSheet = data;
            onSelectedSheetChanged();
        }

        function onItemSelected(item)
        {
            setSelectedSheet(item.getData());
        }

        function onItemRemoved(item)
        {
            item.onRemove.removeEventListener(onItemRemoved);
            item.onSelect.removeEventListener(onItemSelected);

            var data = item.getData();
            removeDataFromLoadedData(data);
            loadedData = computeWeekPriorityIndexes(loadedData);
            
            updateAllItems();

            if (selectedSheet == data)
                that.selectSheetByIndex(0);
        }

        function removeDataFromLoadedData(data)
        {
            if (data == null) return;

            var index = loadedData.indexOf(data);
            if (index < 0) return;

            loadedData.splice(index, 1);
        }

        function computeWeekPriorityIndexes(dataList)
        {
            var sorted = [].slice.call(dataList).sort(function (a, b) { return a.weekPriority > b.weekPriority; })
            for (var i = 0; i < sorted.length; i++)
            {
                sorted[i].weekPriorityIndex = i;
            }

            return sorted;
        }

        function updateAllItems()
        {
            for (var i = 0; i < items.length; i++)
                items[i].update();
        }

        function updateLoadedData()
        {
            loadedData = [].slice.call(loadedData).sort(function (a, b) { return a.weekPriority > b.weekPriority; });
            loadedData = computeWeekPriorityIndexes(loadedData);
        }

        /// Constructor
        (function ()
        {
            addSheetButton.addEventListener("click", () =>
            {
                apiCatalog.add({ ScheduleId: scheduleId }, (response) =>
                {
                    if (response["response"])
                    {
                        that.forceUpdate();
                    }
                    else
                    {
                        console.error(response);
                    }
                });
            });
        })();
    }

    function ScheduleSheetItem(data, sheetManager, maxWeekdayIndex, apiCatalog,  parent, template)
    {
        var that = this;
        var selectedClassName = "sheet-item-selected";

        var rootElement = undefined;
        var blocker;

        var elementReferences = {};
        var controlReferences = {};

        var deleteButton = undefined; 
        var moveDownButton,
            moveUpButton;

        this.onSelect = new EventHandler();
        this.onRemove = new EventHandler();

        this.getData = function ()
        {
            return data;
        };

        this.setSelectedStatus = function (selected)
        {
            if (selected)
            {
                rootElement.classList.add(selectedClassName);
            }
            else
            {
                rootElement.classList.remove(selectedClassName);
            }
        };

        this.remove = function ()
        {
            utils.removeElement(rootElement);
            that.onRemove.invoke(that);
        };

        this.update = function ()
        {
            elementReferences["name"].setText((data.weekPriorityIndex + 1) + " Неделя");
            moveDownButton.setAttribute("active", data.weekPriorityIndex == 0 ? "no" : "");
            moveUpButton.setAttribute("active", data.weekPriorityIndex == maxWeekdayIndex ? "no" : "");
        };

        (function ()
        {
            var templateElement = document.createElement('div');
            templateElement.innerHTML = utils.decodeHtml(template);

            /// Получаем рут элемент
            var root = rootElement = templateElement.firstElementChild;
            blocker = new Blocker(rootElement.querySelector("[blocker]"));

            elementReferences = new ReferenceElementsBuilder("data-ref").build(root);
            controlReferences = new ReferenceElementsBuilder("control").build(root);

            root.addEventListener("click", () => { that.onSelect.invoke(that); });

            moveDownButton = controlReferences["move-down"].element;
            moveDownButton.addEventListener("click", (event) =>
            {
                sendApiMoveRequest(-1);
                event.stopPropagation();
            }, true);

            moveUpButton = controlReferences["move-up"].element;
            moveUpButton.addEventListener("click", (event) =>
            {
                sendApiMoveRequest(1);
                event.stopPropagation();
            }, true);

            deleteButton = controlReferences["delete"].element;
            deleteButton.addEventListener("click", (event) =>
            {
                event.stopPropagation()

                if (!confirm("Вы уверены, что хотите удалить таблицу расписания безвозвратно? Также вместе с таблицой будут удалены все ее записи"))
                    return;

                blocker.setActive(true);
                apiCatalog.delete({ Id: data.id }, function (response)
                {
                    if (response["response"])
                    {
                        that.remove();
                    }
                    else
                    {
                        console.error(response);
                    }
                    blocker.setActive(false);
                });
            }, true);

            parent.appendChild(root);
            that.update();
        })();

        function sendApiMoveRequest(direction)
        {
            blocker.setActive(true);
            apiCatalog.move({ ScheduleSheetId: data.id, Direction: direction }, function (response)
            {
                if (response["response"])
                {
                    replaceSheetWeekdayPriority(response["response"]);
                    sheetManager.forceUpdate(() =>
                    {
                        sheetManager.selectSheetById(data.id);
                    });
                }
                else
                {
                    console.error(response);
                }
                blocker.setActive(false);
            });
        }

        function replaceSheetWeekdayPriority(newSheet)
        {
            var sheets = sheetManager.getAllSheets();

            for (var i = 0; i < sheets.length; i++)
            {
                if (sheets[i].id == newSheet.id)
                {
                    sheets[i].weekPriority = newSheet.weekPriority;
                    return;
                }
            }
        }
    }

    function ScheduleRecordManager(scheduleId, rootElement, listTemplate, itemTemplate, rootBlocker)
    {
        var that = this;
        var elementsReferences = new ReferenceElementsBuilder("data-ref").build(rootElement);
        var controlsReferences = new ReferenceElementsBuilder("control").build(rootElement);
        var scrollbar = rootElement;

        var listElement = rootElement;
        var columns = [];

        var blocker = new Blocker(rootElement.querySelector("[blocker]"));
        var currentSheet = undefined;

        this.setSheet = function (sheet)
        {
            if (currentSheet == sheet) return;
            currentSheet = sheet;

            redrawAll();
        };

        (function ()
        {
            /// Build columens
            for (var i = 0; i < daysOfTheWeek.length; i++)
                columns.push(new ScheduleRecordList(i, listElement, listTemplate, itemTemplate));
        })();

        function redrawAll()
        {
            updateColumns();
            scrollbar.scrollLeft = scrollbar.scrollTop = 0;
        }

        function updateColumns()
        {
            for (var i = 0; i < columns.length; i++)
            {
                columns[i].removeAllRecords();
                columns[i].setSheet(currentSheet);
                columns[i].reset();
            }
        }
    }

    function ScheduleRecordList(weekdayIndex, parent, template, itemTemplate)
    {
        var apiCatalog = window.api.catalog.scheduleRecords;
        var that = this;
        var selectedClassName = "sheet-item-selected";

        var recordItems = [];
        var targetSheet = undefined;

        var createEditRequestRecordTarget = undefined;
        var creatorApplyAction = creator_createAction;
        var targetCreatorRequestMode = creatorRequestMode.create;
        var rootElement,
            creatorElement,
            collectionElement,
            scrollbar;

        var elementReferences = {};
        var controlReferences = {};

        var blocker;
    
        this.reset = function ()
        {
            setCreatorActive(false);
        };

        this.getSheet = function ()
        {
            return targetSheet;
        };

        this.setSheet = function (sheet)
        {
            if (sheet == null || sheet == undefined || sheet == targetSheet) return;

            targetSheet = sheet;
            that.update();
        };

        this.onRemove = new EventHandler();
        
        this.remove = function ()
        {
            utils.removeElement(rootElement);
            that.onRemove.invoke(that);
        };

        this.update = function ()
        {
            elementReferences["weekday"].setText(daysOfTheWeek[weekdayIndex]);

            that.removeAllRecords();

            if (targetSheet != undefined && targetSheet != null)
                that.addRecordRange(targetSheet.records.filter(function (record) { return record.day === weekdayIndex; }));

            updateRecordPositions();
            scrollbar.scrollLeft = 0;
            scrollbar.scrollTop = 0;
        };

        this.addRecord = function (record)
        {
            recordItems.push(new ScheduleRecordItem(that, record, apiCatalog, collectionElement, itemTemplate));
        };

        this.addRecordRange = function (records)
        {
            for (var i = 0; i < records.length; i++)
                that.addRecord(records[i]);
        };

        this.removeAllRecords = function ()
        {
            for (var i = 0; i < recordItems.length; i++)
                recordItems[i].remove();
            recordItems = [];
        };

        this.requestEdit = function (record)
        {
            createEditRequestRecordTarget = record;
            targetCreatorRequestMode = creatorRequestMode.edit;

            prepareCreator();
            setCreatorActive(true);
        };

        this.requestCreate = function ()
        {
            targetCreatorRequestMode = creatorRequestMode.create;

            prepareCreator();
            setCreatorActive(true);
        };

        function updateRecordPositions()
        {
            if (recordItems.length == 0) return;
            var sortedRecords = recordItems.sort((a, b) =>
            {
                aData = a.getData();
                bData = b.getData();
                return aData.startTime != bData.startTime ? aData.startTime > bData.startTime : aData.duration > bData.duration;
            });
            sortedRecords.forEach((item) => { item.moveToEndHierarchy(); });
        }

        function prepareCreator()
        {
            if (targetCreatorRequestMode == creatorRequestMode.create)
            {
                elementReferences["creator-title"].setText("Новая запись");
                controlReferences["creator-apply"].setText("Добавить");
                creatorApplyAction = creator_createAction;
                utils.clearFormParameters(creatorElement);
            }
            else if (targetCreatorRequestMode == creatorRequestMode.edit && createEditRequestRecordTarget != undefined)
            {
                elementReferences["creator-title"].setText("Редактирование записи");
                controlReferences["creator-apply"].setText("Сохранить");
                creatorApplyAction = creator_editAction;
                putCreatorParameters(createEditRequestRecordTarget);
            }
        }

        function setCreatorActive(enabled)
        {
            creatorElement.setAttribute("active", enabled ? "" : "no");
        }

        function hasSheet()
        {
            return !(targetSheet == null || targetSheet == undefined);
        }

        function creator_createAction(parameters, callback)
        {
            apiCatalog.add(parameters, (response) =>
            {
                if (response["response"])
                {
                    if (!hasSheet())
                    {
                        console.error("Sheet is not defined");
                    }
                    else
                    {
                        console.log("Record added", response["response"]);
                        targetSheet.records.push(response["response"]);

                        that.update();
                    }
                }
                else
                {
                    console.error(response);
                }
                callback(response);
                setCreatorActive(false);
            });
        }

        function creator_editAction(parameters, callback)
        {
            parameters.ScheduleRecordId = createEditRequestRecordTarget.id;
            apiCatalog.edit(parameters, (response) =>
            {
                if (response["response"])
                {
                    if (!hasSheet())
                    {
                        console.error("Sheet is not defined");
                    }
                    else
                    {
                        console.log("Record edited", response["response"]);

                        replaceRecord(response["response"]);
                        that.update();
                    }
                }
                else
                {
                    console.error(response);
                }
                callback(response);
                setCreatorActive(false);
            });
        }

        function replaceRecord(newRecord)
        {
            var sheet = that.getSheet();
            for (var i = 0; i < sheet.records.length; i++)
            {
                if (sheet.records[i].id == newRecord.id)
                {
                    sheet.records[i] = newRecord;
                    return;
                }
            }
        }

        function putCreatorParameters(record)
        {
            elementReferences["InstructorId"].element.value = record.instructor.id;
            elementReferences["DisciplineId"].element.value = record.discipline.id;
            elementReferences["DisciplineMethodId"].element.value = record.disciplineMethod.id;
            elementReferences["Location"].element.value = record.location;
            elementReferences["StartTime"].element.value = getTimeStringFromTimeSpan(record.startTime);
            elementReferences["EndTime"].element.value = getTimeStringFromTimeSpan(record.startTime + record.duration);
            console.log(elementReferences);
        }

        (function ()
        {
            var templateElement = document.createElement('div');
            templateElement.innerHTML = utils.decodeHtml(template);

            /// Получаем рут элемент
            var root = rootElement = templateElement.firstElementChild;
            scrollbar = root.querySelector(".scrollbar");

            elementReferences = new ReferenceElementsBuilder("data-ref").build(root);
            controlReferences = new ReferenceElementsBuilder("control").build(root);
            
            collectionElement = elementReferences["collection"].element;

            creatorElement = elementReferences["entity-creator"].element;
            setCreatorActive(false);

            blocker = new Blocker(creatorElement.querySelector("[blocker]"));

            controlReferences["add"].element.addEventListener("click", (event) =>
            {
                that.requestCreate();
            });

            controlReferences["creator-close"].element.addEventListener("click", (event) =>
            {
                setCreatorActive(false);
            });

            controlReferences["creator-apply"].element.addEventListener("click", (event) =>
            {
                if (!hasSheet())
                {
                    console.error("Sheet is not defined");
                    return;
                }

                if (!utils.validateForm(creatorElement))
                {
                    alert("Заполните все поля");
                    return;
                }

                var parameters = utils.getFormParameters(creatorElement);
                parameters.ScheduleSheetId = targetSheet.id;
                parameters.Day = weekdayIndex;
                parameters.Duration = parameters.EndTime - parameters.StartTime;
                delete parameters.EndTime;

                if (parameters.Duration <= 0)
                {
                    alert("Время введено неверно");
                    return;
                }

                blocker.setActive(true);
                creatorApplyAction(parameters, () => { blocker.setActive(false); });
            });

            parent.appendChild(root);
            utils.addSimpleTouchScroller(scrollbar);

            that.update();
        })();
    }

    function ScheduleRecordItem(recordList, recordData, apiCatalog, parent, template)
    {
        var that = this;

        var rootElement = undefined;

        var elementReferences = {};
        var controlReferences = {};

        var deleteButton,
            editButton;
        var blocker;

        this.onRemove = new EventHandler();

        this.getData = function ()
        {
            return recordData;
        };

        this.moveToEndHierarchy = function ()
        {
            rootElement.parentElement.insertBefore(rootElement, null);
        };

        this.remove = function ()
        {
            utils.removeElement(rootElement);
            that.onRemove.invoke(that);
        };

        this.update = function ()
        {
            elementReferences["time"].setText(getTimeStringFromTimeSpan(recordData.startTime) + " - " + getTimeStringFromTimeSpan(recordData.startTime + recordData.duration));
            elementReferences["discipline-method"].setText(recordData.disciplineMethod.name);
            elementReferences["discipline"].setText(recordData.discipline.name);
            elementReferences["instructor"].setText(recordData.instructor.fullName);
            elementReferences["duration"].setText(recordData.duration + " минут");
            elementReferences["location"].setText(recordData.location);
        };

        function removeRecordFromSheet(sheet, record)
        {
            if (sheet == undefined || sheet == null || !Array.isArray(sheet.records)) return;

            var index = sheet.records.indexOf(record);
            if (index < 0) return;

            sheet.records.splice(index, 1);
        }

        (function ()
        {
            var templateElement = document.createElement('div');
            templateElement.innerHTML = utils.decodeHtml(template);

            /// Получаем рут элемент
            var root = rootElement = templateElement.firstElementChild;
            blocker = new Blocker(rootElement.querySelector("[blocker]"));

            elementReferences = new ReferenceElementsBuilder("data-ref").build(root);
            controlReferences = new ReferenceElementsBuilder("control").build(root);
            
            deleteButton = controlReferences["delete"].element;
            deleteButton.addEventListener("click", (event) =>
            {
                event.stopPropagation();

                if (!confirm("Вы уверены, что хотите удалить запись?"))
                    return;

                var sheet = recordList.getSheet();

                blocker.setActive(true);
                apiCatalog.delete({ Id: recordData.id }, function (response)
                {
                    if (response["response"])
                    {
                        removeRecordFromSheet(sheet, recordData);
                        that.remove();
                    }
                    else
                    {
                        console.error(response);
                    }
                    blocker.setActive(false);
                });
            });

            editButton = controlReferences["edit"].element;
            editButton.addEventListener("click", (event) =>
            {
                recordList.requestEdit(recordData);
            });

            root.recordItem = that;

            parent.appendChild(root);
            that.update();
        })();
    }

    function getTimeStringFromTimeSpan(timeSpan)
    {
        var hours = ("0" + Math.floor(timeSpan / 60)).slice(-2);
        var minutes = ("0" + timeSpan % 60).slice(-2);
        return hours + ":" + minutes;
    }
})();