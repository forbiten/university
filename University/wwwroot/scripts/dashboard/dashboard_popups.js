﻿window.dashboard.popup = window.dashboard.popup || {};

/// Windowed popup
(function (context)
{
    var windowed = context.windowed = {};
    var rootElement,
        contentElement,
        loaderElement,
        contentWrapperElement;

    windowed.getRootElement = function () { return rootElement; };
    windowed.getContentElement = function () { return contentElement; };
    windowed.updatePointers = function ()
    {
        rootElement = document.querySelector('[popup="windowed"]');
        contentElement = rootElement.querySelector('[popup-ref="content"]');
        loaderElement = rootElement.querySelector('[popup-ref="loader"]');
        contentWrapperElement = rootElement.querySelector('[popup-ref="content-wrapper"]');
    };

    windowed.loader = {};
    windowed.loader.setActive = function (enabled) { setActiveLoader(enabled); };

    windowed.close = function ()
    {
        clearContent();
        setActive(false);
    };

    windowed.load = function (url, parameters, callback)
    {
        setActiveContent(false);
        callback = utils.normalizeFunction(callback);
        
        setActive(true);

        window.request("post", url, parameters, (response) =>
        {
            setActiveContent(true);
            renderContent(response);
            callback(contentElement);
        });
    }

    function setActive(enabled) {
        rootElement.setAttribute("active", enabled ? "" : "no");
    }

    function setActiveContent(enabled) {
        contentWrapperElement.setAttribute("active", enabled ? "" : "no");
        setActiveLoader(false);
    }

    function setActiveLoader(enabled) {
        loaderElement.setAttribute("active", enabled ? "" : "no");
    }

    function renderContent(content)
    {
        if (!HTMLElement.prototype.isPrototypeOf(contentElement))
        {
            console.warn("Windowed popup have no content element. Content was not rendered");
            return;
        }

        contentElement.innerHTML = content;
    }

    function clearContent()
    {
        renderContent("");
    }

}).call(null, window.dashboard.popup);