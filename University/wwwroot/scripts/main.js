function EventHandler()
{
    var listeners = [];

    this.invoke = function (eventData)
    {
        for (var index = 0; index < listeners.length; index++)
            listeners[index](eventData);
    };

    this.addEventListener = function (listener)
    {
        if (!Function.prototype.isPrototypeOf(listener)) return;
        listeners.push(listener);
    };

    this.removeEventListener = function (listener)
    {
        var index = listeners.indexOf(listener);
        if (index < 0) return;

        listeners.splice(listeners.indexOf(listener), 1);
    };

    this.removeEventListenerAll = function ()
    {
        listeners = [];
    };
}
window.request = function (method, url, parameters, callback)
{
    method = method.toUpperCase();

    if (parameters === null || parameters === undefined)
        parameters = {};

    var s_Parameters = "";

    if (Object.prototype.isPrototypeOf(parameters))
    {
        for (var key in parameters)
        {
            s_Parameters += key + "=" + encodeURIComponent(parameters[key]) + "&";
        }
        s_Parameters = s_Parameters.replace(/&\s*$/, "");
    }

    if (method == "GET") url += "?" + s_Parameters;

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function ()
    {
        if (xhr.readyState != 4) return;
        callback(xhr.responseText, xhr);
    };
    xhr.open(method, url, true);

    if (method == "POST")
    {
        s_Parameters = new FormData();
        Object.keys(parameters).forEach(function (key)
        {
            switch (typeof parameters[key])
            {
                case "string":
                case "number":
                case "boolean":
                    s_Parameters.append(key, parameters[key]);
            }
        });
    }

    xhr.send(method == "POST" ? s_Parameters : null);
};
const daysOfTheWeek = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];

function RefElement(refName, refElement, index)
{
    var that = this;

    this.refName = refName;
    this.element = refElement;
    this.index = index;

    this.setText = function (text)
    {
        that.element.innerHTML = text;
    };
}

function ReferenceElementsBuilder(tag)
{
    this.build = function (rootElement)
    {
        var references = {};
        var allRefElements = rootElement.querySelectorAll("[" + tag + "]");
        var _tmpRefName = "";

        for (var rIndex = 0; rIndex < allRefElements.length; rIndex++)
        {
            _tmpRefName = allRefElements[rIndex].getAttribute(tag);
            references[_tmpRefName] = new RefElement(_tmpRefName, allRefElements[rIndex], rIndex);
        }

        return references;
    };
}

window.app = {};
(function (app)
{
    function ElementRoute(elementId)
    {
        var that = this;
        var element = undefined;

        this.getElement = function ()
        {
            if (!HTMLElement.prototype.isPrototypeOf(element))
                that.update();

            return element;
        };

        this.update = function ()
        {
            element = document.getElementById(elementId);
        }
    }

    app.pageRendererContainerRoute = new ElementRoute("page-renderer-container");

})(window.app);