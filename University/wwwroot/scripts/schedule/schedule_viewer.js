﻿const schedule = {};
(function (context)
{
    context.viewer = new ScheduleViewer();

    context.viewer.createConfiguration = () => { return new ScheduleViewerConfiguration(); };

    function ScheduleViewerConfiguration()
    {
        this.rootElement;
        this.displayModeElement;
        this.scheduleTypeModeElement;
        this.collectionRendererElement;
        this.noRenderElement;
        this.entityFilter;
        this.displayModeFilter;

        this.dateNow = 0;
        this.scheduleWeekCountdownDate = {
            month: 0,
            date: 0
        };

        this.recordItemTemplate = "";
        this.recordListTemplate = "";
        this.modeItemTemplate = "";
    }

    function ScheduleViewer()
    {
        var apiCatalog = window.api.catalog.schedules;

        var dateNow;
        var dateStart;
        var that = this;
        var cfg; 

        var filterModes = {
            "groups": groupFilterPerformer,
            "instructors": instructorFilterPerformer
        };

        var blocker;
        var rendererLists = [];
        var schedules = [];
        var scheduleCache = [];

        var scheduleRecordManager;
        var sheetManager;
        var scheduleTypeModeManager;

        var headerDescElement;

        var targetSchedule;
        var targetSheet;

        this.update = function ()
        {
            disableAllRendererLists();
            showRendererView(schedules.length > 0, "Не найдено ни одного расписания");

            sheetManager.setSchedule(targetSchedule);
            scheduleTypeModeManager.setSchedulesQueue(schedules);

            renderSchedule();

            headerDescElement.html(targetSchedule != undefined && targetSheet != null && targetSchedule.sheets.length > 1
                ? "Номер текущей недели: " + (getScheduleWeekIndexAtStart() + 1) 
                : "");
        };
        
        this.configure = function (configuration)
        {
            deleteAllRendererLists();

            cfg = configuration;
            cfg.entityFilter.on("change", onEntitiFilterChanged);

            dateNow = moment(cfg.dateNow);
            configureMoment(dateNow);
            dateStart = moment(cfg.dateNow).month(cfg.scheduleWeekCountdownDate.month).date(cfg.scheduleWeekCountdownDate.date);
            configureMoment(dateStart);

            blocker = new Blocker(cfg.rootElement.find("[blocker]")[0]);
            scheduleRecordManager = new ScheduleRecordManager(cfg.collectionRendererElement[0], cfg.recordListTemplate, cfg.recordItemTemplate, blocker);

            headerDescElement = cfg.rootElement.find("[data-ref='schedule-header-desc']");

            sheetManager = new SheetToggleManager(cfg.displayModeElement, cfg.modeItemTemplate);
            sheetManager.setSchedule(undefined);
            sheetManager.onChange.addEventListener(setTargetSheet);

            scheduleTypeModeManager = new ScheduleTypeToggleManager(cfg.scheduleTypeModeElement, cfg.modeItemTemplate);
            scheduleTypeModeManager.setSchedulesQueue([]);
            scheduleTypeModeManager.onChange.addEventListener(setTargetSchedule);

            showRendererView(false, "Для отображения расписания необходимо выбрать фильтр");
        };

        function configureMoment(momentObject)
        {
            momentObject._locale._week.dow = 1;
        }

        function setTargetSchedule(scheduleId)
        {
            targetSchedule = schedules.find(schedule => schedule.id == scheduleId);
            sortScheduleSheets();

            setTargetSheet("today");
        }

        function setTargetSheet(sheetId)
        {
            if (targetSchedule == undefined) return;

            var sheets = [].slice.call(targetSchedule.sheets);

            if (sheetId == "today")
            {
                sheetId = sheets[getScheduleWeekIndexAtStart()].id;
            }

            targetSheet = sheets.find(sheet => sheet.id == sheetId);
            that.update();
        }

        function getScheduleWeekIndexAtStart()
        {
            var length = (targetSchedule == undefined ? 0 : targetSchedule.sheets == undefined ? 0 : targetSchedule.sheets.length);
            return Math.abs(dateNow.week() - dateStart.week()) % length;
        }

        function getWeekIndexAtStart(length)
        {
            return Math.abs(dateNow.week() - dateStart.week()) % length;
        }

        function getWeekNumberAtStart()
        {
            return Math.abs(dateNow.week() - dateStart.week());
        }

        function renderSchedule()
        {
            if (targetSchedule == undefined) return;
            scheduleRecordManager.setSheet(targetSheet);
        }

        function deleteAllRendererLists()
        {
            for (var index in rendererLists)
                rendererLists[index].remove();
            rendererLists = [];
        }

        function disableAllRendererLists()
        {
            for (var index in rendererLists)
                rendererLists[index].setActive(false);
        }

        function getFreeRendererList()
        {
            for (var index in rendererLists)
            {
                if (!rendererLists.isActive())
                {
                    rendererLists.setActive(true);
                    return rendererLists[index];
                }
            }

            var newRendererList = new RendrereList(cfg.recordListTemplate);
            newRendererList.setActive(true);

            rendererLists.push(newRendererList);

            return newRendererList;
        }

        function groupFilterPerformer(groupId)
        {
            requestSchedules({ StudentGroupId: groupId });
        }

        function instructorFilterPerformer(instructorId)
        {
            requestSchedules({ InstructorId: instructorId });
        }

        function requestSchedules(parameters)
        {
            schedules = [];

            var cachedSchedules = getSchedulesFromCache(parameters);
            if (cachedSchedules != undefined)
            {
                schedules = cachedSchedules;
                selectDefaultSchedule();
                that.update();
                return;
            }

            blocker.setActive(true);

            apiCatalog.getPublic(parameters, (response) =>
            {
                blocker.setActive(false);

                if (response["response"])
                {
                    cacheSchedules(parameters, response["response"]);
                    schedules = response["response"];
                }
                else
                {
                    console.error(response);
                }

                selectDefaultSchedule();
                that.update();
            });
        }

        function selectDefaultSchedule()
        {
            targetSchedule = schedules == undefined ? undefined : schedules[0];

            sortScheduleSheets();
            targetSheet = targetSchedule == undefined ? undefined : targetSchedule.sheets[getScheduleWeekIndexAtStart()];
        }

        function cacheSchedules(parameters, schedules)
        {
            scheduleCache.push({
                parameters: parameters,
                schedules: schedules
            });
        }

        function getSchedulesFromCache(parameters)
        {
            var cacheItem = {};
            var entryNotValid = false;
            for (var index in scheduleCache)
            {
                entryNotValid = false;
                cacheItem = scheduleCache[index];

                for (var pIndex in parameters)
                {
                    if (parameters[pIndex] != cacheItem.parameters[pIndex])
                        entryNotValid = true;
                }

                if (!entryNotValid)
                    return cacheItem.schedules;
            }

            return undefined;
        }

        function onEntitiFilterChanged(event)
        {
            var optionElement = $(this.options[this.selectedIndex]);
            var optionValue = optionElement.val();
            if (optionValue == "")
            {
                schedules = [];
                that.update();
                return;
            }

            var optgroupElement = optionElement.closest('optgroup');
            var filter = optgroupElement.attr('filter');

            var filterMode = filterModes[filter];
            filterMode(optionValue);
        }

        function sortScheduleSheets()
        {
            if (targetSchedule == undefined || targetSchedule.sheets == undefined) return;
            targetSchedule.sheets = targetSchedule.sheets.sort((a, b) => a.weekPriority > b.weekPriority);
        }

        function showRendererView(enabled, message)
        {
            cfg.noRenderElement.attr("active", enabled ? "no" : "");
            cfg.noRenderElement.find("[message-renderer]").html(message);
        };
    }

    function ToggleManager(rootElement, itemTemplate)
    {
        var that = this;
        
        var items = [];

        var parentFilter = rootElement.closest(".schedule-presentation-filter-field");

        rootElement.on('change', onToggleCahnge);

        this.onChange = new EventHandler();

        this.activateToggle = function (index)
        {
            var activeItems = [];
            for (var i in allCreatedToggles)
            {
                if (!allCreatedToggles[i].isActive()) continue;
                activeItems.push(allCreatedToggles[i]);
            }
        };

        this.setActive = function (enabled)
        {
            parentFilter.attr("active", enabled ? "" : "no");
        };

        this.createToggleElement = function()
        {
            return createToggleElement();
        }
        
        this.createToggle = function (value, name)
        {
            var item = createToggleElement();

            item.setValue(value);
            item.setName(name);

            items.push(item);
        }

        this.removeToggles = function ()
        {
            for (var index in items)
                items[index].remove();

            items = [];
            that.refresh();
        }

        this.refresh = function ()
        {
            rootElement.selectpicker("refresh");
        }

        function createToggleElement()
        {
            var toggle = new ToggleItem(itemTemplate, rootElement);
            return toggle;
        }

        function onToggleCahnge(event)
        {
            var optionElement = $(this.options[this.selectedIndex]);
            var optionValue = optionElement.val();

            that.onChange.invoke(optionValue);
        }
    }

    function SheetToggleManager(rootElement, itemTemplate)
    {
        ToggleManager.call(this, rootElement, itemTemplate);

        var todayToggle = this.createToggleElement();
        todayToggle.setValue("today");
        todayToggle.setName("Текущая неделя");

        var previousSchedule;

        this.setSchedule = function (schedule)
        {
            var canDraw = schedule != undefined && schedule.sheets.length != 0;
            this.setActive(canDraw);

            if (previousSchedule == schedule)
                return;

            previousSchedule = schedule;

            this.removeToggles();

            if (!canDraw || schedule.sheets.length == 1) return;

            var sortedSheets = schedule.sheets.sort((a, b) => a.weekPriority > b.weekPriority);

            var counter = 0;
            for (var index in sortedSheets)
            {
                counter++;
                this.createToggle(sortedSheets[index].id, (counter) + " неделя (" + sortedSheets[index].records.length + ")");
            }

            this.refresh();
        };
    }

    function ScheduleTypeToggleManager(rootElement, itemTemplate)
    {
        ToggleManager.call(this, rootElement, itemTemplate);

        var previousQueue;

        this.setSchedulesQueue = function (queue)
        {
            var canDraw = queue != undefined && queue.length > 1;
            this.setActive(canDraw);

            if (previousQueue == queue)
                return;

            previousQueue = queue;

            this.removeToggles();
            
            if (!canDraw) return;

            for (var index in queue)
                this.createToggle(queue[index].id, queue[index].scheduleType.name + " (" + queue[index].sheets.length + ")");

            this.refresh();
        };
    }

    function ToggleItem(template, collectionElement)
    {
        var templateElement = document.createElement('div');
        templateElement.innerHTML = utils.decodeHtml(template);
        
        /// Получаем рут элемент
        var root = templateElement.firstElementChild;
        var jRoot = $(root);

        this.remove = function ()
        {
            jRoot.remove();
        };

        this.getRoot = function ()
        {
            return jRoot;
        };
        
        this.setValue = function (value)
        {
            jRoot.attr("value", value);
        };

        this.setName = function (name)
        {
            jRoot.html(name);
        };

        collectionElement.append(root);
    }

    function ScheduleRecordManager(listElement, listTemplate, itemTemplate, rootBlocker)
    {
        var that = this;
        var columns = [];

        var currentSheet = undefined;

        this.setSheet = function (sheet)
        {
            if (currentSheet == sheet) return;
            currentSheet = sheet;

            redrawAll();
        };

        (function ()
        {
            /// Build columens
            for (var i = 0; i < daysOfTheWeek.length; i++)
                columns.push(new ScheduleRecordList(i, listElement, listTemplate, itemTemplate));
        })();

        function redrawAll()
        {
            updateColumns();
        }

        function updateColumns()
        {
            for (var i = 0; i < columns.length; i++)
            {
                columns[i].removeAllRecords();
                columns[i].setSheet(currentSheet);
            }
        }
    }

    function ScheduleRecordList(weekdayIndex, parent, template, itemTemplate)
    {
        var that = this;

        var recordItems = [];
        var targetSheet = undefined;
        
        var rootElement,
            collectionElement;

        var elementReferences = {};
        var controlReferences = {};
        
        this.getSheet = function ()
        {
            return targetSheet;
        };

        this.setSheet = function (sheet)
        {
            if (sheet == null || sheet == undefined || sheet == targetSheet) return;

            targetSheet = sheet;
            that.update();
        };

        this.onRemove = new EventHandler();

        this.remove = function ()
        {
            utils.removeElement(rootElement);
            that.onRemove.invoke(that);
        };

        this.update = function ()
        {
            elementReferences["header-title"].setText(daysOfTheWeek[weekdayIndex]);

            that.removeAllRecords();

            if (targetSheet != undefined && targetSheet != null)
                that.addRecordRange(targetSheet.records.filter(function (record) { return record.day === weekdayIndex; }));

            updateRecordPositions();
        };

        this.addRecord = function (record)
        {
            recordItems.push(new ScheduleRecordItem(that, record, collectionElement, itemTemplate));
        };

        this.addRecordRange = function (records)
        {
            for (var i = 0; i < records.length; i++)
                that.addRecord(records[i]);
        };

        this.removeAllRecords = function ()
        {
            for (var i = 0; i < recordItems.length; i++)
                recordItems[i].remove();
            recordItems = [];
        };

        function updateRecordPositions()
        {
            if (recordItems.length == 0) return;
            var sortedRecords = recordItems.sort((a, b) =>
            {
                aData = a.getData();
                bData = b.getData();
                return aData.startTime != bData.startTime ? aData.startTime > bData.startTime : aData.duration > bData.duration;
            });
            sortedRecords.forEach((item) => { item.moveToEndHierarchy(); });
        }
        
        (function ()
        {
            var templateElement = document.createElement('div');
            templateElement.innerHTML = utils.decodeHtml(template);

            /// Получаем рут элемент
            var root = rootElement = templateElement.firstElementChild;
            scrollbar = root.querySelector(".scrollbar");

            elementReferences = new ReferenceElementsBuilder("data-ref").build(root);
            controlReferences = new ReferenceElementsBuilder("control").build(root);

            collectionElement = elementReferences["collection-renderer"].element;
            
            parent.appendChild(root);
            utils.addSimpleTouchScroller(root.querySelector("[scrollbar]"));

            that.update();
        })();
    }

    function ScheduleRecordItem(recordList, recordData, parent, template)
    {
        var that = this;

        var rootElement = undefined;

        var elementReferences = {};
        var controlReferences = {};
        
        this.onRemove = new EventHandler();

        this.getData = function ()
        {
            return recordData;
        };

        this.moveToEndHierarchy = function ()
        {
            rootElement.parentElement.insertBefore(rootElement, null);
        };

        this.remove = function ()
        {
            utils.removeElement(rootElement);
            that.onRemove.invoke(that);
        };

        this.update = function ()
        {
            elementReferences["time"].setText(getTimeStringFromTimeSpan(recordData.startTime) + " - " + getTimeStringFromTimeSpan(recordData.startTime + recordData.duration));
            elementReferences["discipline-method"].setText(recordData.disciplineMethod.name);
            elementReferences["discipline"].setText(recordData.discipline.name);
            elementReferences["instructor"].setText(recordData.instructor.fullName);
            elementReferences["address"].setText(recordData.location);
        };
        
        (function ()
        {
            var templateElement = document.createElement('div');
            templateElement.innerHTML = utils.decodeHtml(template);

            /// Получаем рут элемент
            var root = rootElement = templateElement.firstElementChild;

            elementReferences = new ReferenceElementsBuilder("data-ref").build(root);
            controlReferences = new ReferenceElementsBuilder("control").build(root);
            
            root.recordItem = that;

            parent.appendChild(root);
            that.update();
        })();
    }

    function getTimeStringFromTimeSpan(timeSpan)
    {
        var hours = ("0" + Math.floor(timeSpan / 60)).slice(-2);
        var minutes = ("0" + timeSpan % 60).slice(-2);
        return hours + ":" + minutes;
    }
})(schedule);