﻿function Blocker(rootElement)
{
    this.getRootElement = function ()
    {
        return rootElement;
    };

    this.setActive = function (enabled)
    {
        rootElement.setAttribute("active", enabled ? "" : "no");
    };
}