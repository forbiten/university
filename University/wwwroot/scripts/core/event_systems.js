﻿function EventHandler()
{
    var listeners = [];

    this.invoke = function (eventData)
    {
        for (var index = 0; index < listeners.length; index++)
            listeners[index](eventData);
    };

    this.addEventListener = function (listener)
    {
        if (!Function.prototype.isPrototypeOf(listener)) return;
        listeners.push(listener);
    };

    this.removeEventListener = function (listener)
    {
        var index = listeners.indexOf(listener);
        if (index < 0) return;

        listeners.splice(listeners.indexOf(listener), 1);
    };

    this.removeEventListenerAll = function ()
    {
        listeners = [];
    };
}