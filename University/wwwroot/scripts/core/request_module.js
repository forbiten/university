﻿window.request = function (method, url, parameters, callback)
{
    method = method.toUpperCase();

    if (parameters === null || parameters === undefined)
        parameters = {};

    var s_Parameters = "";

    if (Object.prototype.isPrototypeOf(parameters))
    {
        for (var key in parameters)
        {
            s_Parameters += key + "=" + encodeURIComponent(parameters[key]) + "&";
        }
        s_Parameters = s_Parameters.replace(/&\s*$/, "");
    }

    if (method == "GET") url += "?" + s_Parameters;

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function ()
    {
        if (xhr.readyState != 4) return;
        callback(xhr.responseText, xhr);
    };
    xhr.open(method, url, true);

    if (method == "POST")
    {
        s_Parameters = new FormData();
        Object.keys(parameters).forEach(function (key)
        {
            switch (typeof parameters[key])
            {
                case "string":
                case "number":
                case "boolean":
                    s_Parameters.append(key, parameters[key]);
            }
        });
    }

    xhr.send(method == "POST" ? s_Parameters : null);
};