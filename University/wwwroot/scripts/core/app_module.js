﻿const daysOfTheWeek = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'];

function RefElement(refName, refElement, index)
{
    var that = this;

    this.refName = refName;
    this.element = refElement;
    this.index = index;

    this.setText = function (text)
    {
        that.element.innerHTML = text;
    };
}

function ReferenceElementsBuilder(tag)
{
    this.build = function (rootElement)
    {
        var references = {};
        var allRefElements = rootElement.querySelectorAll("[" + tag + "]");
        var _tmpRefName = "";

        for (var rIndex = 0; rIndex < allRefElements.length; rIndex++)
        {
            _tmpRefName = allRefElements[rIndex].getAttribute(tag);
            references[_tmpRefName] = new RefElement(_tmpRefName, allRefElements[rIndex], rIndex);
        }

        return references;
    };
}

window.app = {};
(function (app)
{
    function ElementRoute(elementId)
    {
        var that = this;
        var element = undefined;

        this.getElement = function ()
        {
            if (!HTMLElement.prototype.isPrototypeOf(element))
                that.update();

            return element;
        };

        this.update = function ()
        {
            element = document.getElementById(elementId);
        }
    }

    app.pageRendererContainerRoute = new ElementRoute("page-renderer-container");

})(window.app);