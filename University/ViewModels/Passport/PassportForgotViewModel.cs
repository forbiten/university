﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class PassportForgotViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
