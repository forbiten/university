﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class PassportResetPasswordViewModel : BasePassportRegisterViewModel
    {
        [Required]
        public string Code { get; set; }
    }
}
