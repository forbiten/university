﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database;
using University.Database.Models;

namespace University.ViewModels
{
    public class ApiScheduleSheetsGetResponseViewModel
    {
        public readonly string Id;
        public readonly string ScheduleId;
        public readonly int WeekPriority;
        public readonly int WeekPriorityIndex;
        public readonly List<ApiScheduleRecordGetResponseViewModel> Records = new List<ApiScheduleRecordGetResponseViewModel>();

        public ApiScheduleSheetsGetResponseViewModel(ScheduleSheet scheduleSheet)
        {
            Id = scheduleSheet.Id;
            ScheduleId = scheduleSheet.ScheduleId;
            WeekPriority = scheduleSheet.WeekPriority;
            WeekPriorityIndex = scheduleSheet.GetWeekPriorityIndex();

            if (scheduleSheet.Records != null)
            {
                foreach (var record in scheduleSheet.Records)
                    Records.Add(new ApiScheduleRecordGetResponseViewModel(record));
            }
        }
    }
}
