﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiScheduleSheetsAddViewModel
    {
        [Required]
        public string ScheduleId { get; set; }

        public int? WeekPriority { get; set; }
    }
}
