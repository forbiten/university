﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class ApiScheduleSheetsMoveViewModel
    {
        [Required]
        public string ScheduleSheetId { get; set; }

        [Required]
        public int Direction { get; set; }
    }
}
