﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiDisciplineMethodAddViewModel
    {
        [Required]
        [Display(Name = "Наименование метода дисциплины")]
        [StringLength(100, ErrorMessage = "{0} должно быть не менее {2} и не более {1} символов.", MinimumLength = 1)]
        public string Name { get; set; }
    }
}
