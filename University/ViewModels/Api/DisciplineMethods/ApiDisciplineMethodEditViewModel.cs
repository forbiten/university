﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiDisciplineMethodEditViewModel : ApiDisciplineMethodAddViewModel
    {
        [Required]
        public string DisciplineMethodId { get; set; }
    }
}
