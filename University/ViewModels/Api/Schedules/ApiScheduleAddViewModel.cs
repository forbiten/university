﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class ApiScheduleAddViewModel
    {
        [Required]
        public string StudentGroupId { get; set; }

        [Required]
        public string ScheduleTypeId { get; set; }
    }
}
