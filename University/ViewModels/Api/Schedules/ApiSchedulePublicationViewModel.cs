﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class ApiSchedulePublicationViewModel
    {
        [Required]
        public string ScheduleId { get; set; }

        [Required]
        public PublicationStatus State { get; set; }
    }
}
