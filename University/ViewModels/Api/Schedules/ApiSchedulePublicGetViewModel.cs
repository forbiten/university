﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database;
using University.Database.Models;

namespace University.ViewModels
{
    public class ApiSchedulePublicGetViewModel
    {
        public string InstructorId { get; set; }
        public string StudentGroupId { get; set; }
    }
}
