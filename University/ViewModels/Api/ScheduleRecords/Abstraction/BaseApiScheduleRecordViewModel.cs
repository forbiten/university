﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class BaseApiScheduleRecordViewModel
    {
        [Required]
        public string InstructorId { get; set; }

        [Required]
        public string DisciplineId { get; set; }

        [Required]
        public string DisciplineMethodId { get; set; }

        [Required]
        public int StartTime { get; set; }

        [Required]
        public int Duration { get; set; }

        [Required]
        public DayOfWeek Day { get; set; }

        [Required]
        public string Location { get; set; }
    }
}
