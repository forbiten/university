﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class SchedulePageDisplayViewModel
    {
        public readonly StudentGroup[] StudentGroups;
        public readonly Instructor[] Instructors;
        public readonly ScheduleConfiguration ScheduleConfiguration;

        public SchedulePageDisplayViewModel(
            StudentGroup[] studentGroups,
            Instructor[] instructors,
            ScheduleConfiguration scheduleConfiguration)
        {
            StudentGroups = studentGroups;
            Instructors = instructors;
            ScheduleConfiguration = scheduleConfiguration;
        }
    }
}
