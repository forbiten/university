﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class DashboardStudentGroupManagerDisplayViewModel
    {
        public readonly Qualification[] Qualifications;
        public readonly EducationSpecialty[] Specialties;
        public readonly EducationMethod[] EducationMethods;

        public DashboardStudentGroupManagerDisplayViewModel(
            Qualification[] qualifications, 
            EducationSpecialty[] specialties, 
            EducationMethod[] educationMethods)
        {
            Qualifications = qualifications;
            Specialties = specialties;
            EducationMethods = educationMethods;
        }
    }
}
