﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.ViewModels
{
    public class DashboardScheduleEditorDisplayViewModel
    {
        public readonly Schedule Schedule;
        public readonly Discipline[] Disciplines;
        public readonly DisciplineMethod[] DisciplineMethods;
        public readonly Instructor[] Instructors;

        public DashboardScheduleEditorDisplayViewModel(
            Schedule schedule,
            Discipline[] disciplines,
            DisciplineMethod[] disciplineMethods,
            Instructor[] instructors)
        {
            Schedule = schedule;
            Disciplines = disciplines;
            DisciplineMethods = disciplineMethods;
            Instructors = instructors;
        }
    }
}
