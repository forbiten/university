﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace University.ViewModels
{
    public class NavigationItemViewModel
    {
        public string Link { get; set; }
        public string Name { get; set; }
        public string IconName { get; set; }
    }
}
