﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using University.Communication;
using University.Services;
using System;
using Microsoft.Extensions.Logging;
using University.Database.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using System.Reflection;
using University.Authorization;
using Microsoft.AspNetCore.Http;
using University.Database;
using System.Threading.Tasks;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.Net.Http.Headers;

namespace University
{
    public class Startup
    {
        private readonly ILogger<Startup> m_Logger;

        public Startup(ILogger<Startup> logger, IConfiguration configuration)
        {
            m_Logger = logger;
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // Метод настройки сервисов приложения
        public void ConfigureServices(IServiceCollection services)
        {
            /// Регистрируем конфиг администратора
            var administrationCfg = new AdministrationConfiguration();
            Configuration.Bind("Administration", administrationCfg);
            services.AddSingleton(administrationCfg);

            /// Регистрируем конфиг расписания
            var scheduleCfg = new ScheduleConfiguration();
            Configuration.Bind("Schedule", scheduleCfg);
            services.AddSingleton(scheduleCfg);

            services.AddResponseCompression();

            services.AddDbContext<UniversityDbContext>(options =>
                options.UseNpgsql(Configuration.GetConnectionString("UniversityConnection"), x => x.MigrationsAssembly("University.Core")));

            /// Регистрируем все доступные менеджеры приложения
            services.AddAllUniversityManagers<UniversityDbContext>();

            services.AddIdentity<User, IdentityRole>(options => 
            {
                options.Password.RequireNonAlphanumeric = false;
                options.User.RequireUniqueEmail = true;

                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
            })
            .AddEntityFrameworkStores<UniversityDbContext>()
            .AddDefaultTokenProviders();

            services.AddAuthentication()
                /// Аутентификация через Google
                .AddGoogle(options => Configuration.ConfigureGoogleAuthenticationOptions(options))
                /// Аутентификация через Facebook
                .AddFacebook(options => Configuration.ConfigureFacebookAuthenticationOptions(options))
                /// Аутентификация через Twitter
                .AddTwitter(options => Configuration.ConfigureTwitterAuthenticationOptions(options));

            services.ConfigureApplicationCookie(options => 
            {
                options.LoginPath = "/passport/login";
                options.AccessDeniedPath = "/security/denied";
            });

            services.AddEmailSender(Configuration.GetEmailSenderOptions());
            services.AddMvc();
            services.AddRouting(options => options.LowercaseUrls = true);

            /// Добавление политики авторизации
            services.AddConstantAuthorizationPolicy();
        }

        // Метод настройки пайплайна http запросов
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider services)
        {
            services.GetService<UniversityDbContext>().Database.Migrate();

            /// Добавляем константную политику авторизации      
            var rsvc = services.AddConstantRoles<UniversityDbContext>().Result;
            RegisterAdministrators(services).Wait();

            app.UseResponseCompression();

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error?code={0}");
            }

            //app.UseStatusCodePagesWithRedirects("/error?code={0}");
            app.UseStatusCodePages();

            var resourcesCachedDuration = TimeSpan.FromDays(7).TotalSeconds;
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] = $"public,max-age={resourcesCachedDuration}";
                }
            });

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseAuthentication();
            
            app.UseMvc();

        }

        private async Task RegisterAdministrators(IServiceProvider services)
        {
            using (var scope = services.CreateScope())
            {
                var scopedProvider = scope.ServiceProvider;
                var config = scopedProvider.GetService<AdministrationConfiguration>();
                var userManager = scopedProvider.GetService<UserManager<User>>();
                var roleManager = scopedProvider.GetService<RoleManager<IdentityRole>>();

                foreach (var adminEmail in config.Admins)
                {
                    var user = await userManager.FindByEmailAsync(adminEmail);

                    if (user == null || await userManager.IsInRoleAsync(user, RoleConstants.RoleAdministrator)) continue;

                    await userManager.AddToRoleAsync(user, RoleConstants.RoleAdministrator);
                }
            }
        }
    }
}
