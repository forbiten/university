﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace University.Communication
{
    public interface IEmailSender
    {
        MailAddress DefaultSender { get; }
        Task SendEmailAsync(MailAddress from, MailAddress to, string subject, string message);
        Task SendEmailWithDefaultSenderAsync(MailAddress to, string subject, string message);
    }
}
