﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace University.Communication
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailAsync(this IEmailSender sender, string from, string to, string subject, string message)
        {
            return sender.SendEmailAsync(new MailAddress(from), new MailAddress(to), subject, message);
        }

        public static Task SendEmailWithDefaultSenderAsync(this IEmailSender sender, string to, string subject, string message)
        {
            return sender.SendEmailWithDefaultSenderAsync(new MailAddress(to), subject, message);
        }
    }
}
