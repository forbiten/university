﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace University.Migrations
{
    public partial class ChangedRecords3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleRecords_ScheduleSheets_ScheduleSheetId",
                table: "ScheduleRecords");

            migrationBuilder.AlterColumn<string>(
                name: "ScheduleSheetId",
                table: "ScheduleRecords",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleRecords_ScheduleSheets_ScheduleSheetId",
                table: "ScheduleRecords",
                column: "ScheduleSheetId",
                principalTable: "ScheduleSheets",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleRecords_ScheduleSheets_ScheduleSheetId",
                table: "ScheduleRecords");

            migrationBuilder.AlterColumn<string>(
                name: "ScheduleSheetId",
                table: "ScheduleRecords",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleRecords_ScheduleSheets_ScheduleSheetId",
                table: "ScheduleRecords",
                column: "ScheduleSheetId",
                principalTable: "ScheduleSheets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
