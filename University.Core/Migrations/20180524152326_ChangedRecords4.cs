﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace University.Migrations
{
    public partial class ChangedRecords4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleRecords_ScheduleSheets_ScheduleSheetId",
                table: "ScheduleRecords");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleRecords_ScheduleSheets_ScheduleSheetId",
                table: "ScheduleRecords",
                column: "ScheduleSheetId",
                principalTable: "ScheduleSheets",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ScheduleRecords_ScheduleSheets_ScheduleSheetId",
                table: "ScheduleRecords");

            migrationBuilder.AddForeignKey(
                name: "FK_ScheduleRecords_ScheduleSheets_ScheduleSheetId",
                table: "ScheduleRecords",
                column: "ScheduleSheetId",
                principalTable: "ScheduleSheets",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
