﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.OAuth;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;

namespace University.Authentication.VK
{
    public class VKOptions : OAuthOptions
    {
        public VKOptions()
        {
            CallbackPath = new PathString("/signin-vkontakte");
            AuthorizationEndpoint = VKDefaults.AuthorizationEndpoint;
            TokenEndpoint = VKDefaults.TokenEndpoint;
            UserInformationEndpoint = VKDefaults.UserInformationEndpoint;

            Scope.Add("email");

            Fields.Add("uid");
            Fields.Add("first_name");
            Fields.Add("last_name");
            Fields.Add("screen_name");
            Fields.Add("email");

            ClaimActions.MapJsonKey(ClaimTypes.NameIdentifier, "uid");
            ClaimActions.MapJsonKey(ClaimTypes.Name, "screen_name");
            ClaimActions.MapJsonKey(ClaimTypes.GivenName, "first_name");
            ClaimActions.MapJsonKey(ClaimTypes.Surname, "last_name");
            ClaimActions.MapJsonKey(ClaimTypes.Email, "email");
        }

        public ISet<string> Fields { get; } = new HashSet<string>();
        public string Version { get; set; }
    }
}