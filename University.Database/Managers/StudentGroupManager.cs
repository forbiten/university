﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.Database
{
    public class StudentGroupManager : Manager<StudentGroup>
    {
        public StudentGroupManager(DbContext context) : base(context)
        {
        }
    }
}
