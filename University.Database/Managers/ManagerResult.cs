﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Database
{
    public struct ManagerResult<T>
        where T : class
    {
        public bool Succeeded { get; private set; }
        public T Entity { get; private set; }
        public string ErrorMessage { get; set; }

        public ManagerResult(T entity, bool status, string errorMessage = null)
        {
            Entity = entity;
            Succeeded = status;
            ErrorMessage = errorMessage;
        }
    }
}
