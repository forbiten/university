﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using University.Database.Models;

namespace University.Database
{
    public class ScheduleSheetManager : Manager<ScheduleSheet>
    {
        public ScheduleSheetManager(DbContext context) : base(context)
        {
        }
    }
}
