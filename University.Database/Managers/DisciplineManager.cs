﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public class DisciplineManager : Manager<Discipline>
    {
        public DisciplineManager(DbContext context) : base(context)
        {
        }
    }
}
