﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public class InstructorManager : Manager<Instructor>
    {
        public InstructorManager(DbContext context) : base(context)
        {
        }
    }
}
