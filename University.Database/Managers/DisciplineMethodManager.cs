﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using University.Database.Models;

namespace University.Database
{
    public class DisciplineMethodManager : Manager<DisciplineMethod>
    {
        public DisciplineMethodManager(DbContext context) : base(context)
        {
        }
    }
}
