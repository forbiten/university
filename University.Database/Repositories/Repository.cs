﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using University.Database.Models;

namespace University.Database
{
    public class Repository<T, TContext>
        where T : Entity
        where TContext : DbContext
    {
        public readonly TContext Context;
        public DbSet<T> DbSet => Context.Set<T>();

        public Repository(TContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Return add entity status
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Add(T entity)
        {
            /*
            if (DbSet.Contains(entity))
                return false;
            */

            try
            {
                var result = DbSet.Add(entity);
                return result.State == EntityState.Added;
            }
            catch (Exception)
            {
                return false;
            }
            
        }

        /// <summary>
        /// Return async add entity status
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task<bool> AddAsync(T entity)
        {
            /*
            if (await DbSet.ContainsAsync(entity))
                return false;
            */

            try
            {
                var result = await DbSet.AddAsync(entity);
                return result.State == EntityState.Added;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Get all entities
        /// </summary>
        /// <returns></returns>
        public T[] Get()
        {
            return DbSet.ToArray();
        }

        /// <summary>
        /// Get all entities async
        /// </summary>
        /// <returns></returns>
        public Task<T[]> GetAsync()
        {
            return DbSet.ToArrayAsync();
        }

        /// <summary>
        /// Return all predicted entities
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate).AsEnumerable();
        }

        /// <summary>
        /// Return first or default match async
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate)
        {
            return DbSet.FirstOrDefaultAsync(predicate);
        }

        /// <summary>
        /// Return first or default match
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            return DbSet.FirstOrDefault(predicate);
        }

        /// <summary>
        /// Return remove entity status
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool Remove(T entity)
        {
            if (entity == null) return false;
            return DbSet.Remove(entity).State == EntityState.Deleted;
        }
        
        /// <summary>
        /// Update entity
        /// </summary>
        /// <param name="entity"></param>
        public void Update(T entity)
        {
            Context.Update(entity);
            //Context.Entry(entity).State = EntityState.Modified;
            //Context.Set<T>().Attach(entity);
        }

        /// <summary>
        /// Save changes
        /// </summary>
        /// <returns></returns>
        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        /// <summary>
        /// Save changes async
        /// </summary>
        /// <returns></returns>
        public Task<int> SaveChangesAsync()
        {
            return Context.SaveChangesAsync();
        }
    }
}
