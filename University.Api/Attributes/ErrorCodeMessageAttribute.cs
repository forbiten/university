﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Api
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ErrorCodeMessageAttribute : Attribute
    {
        public string message;

        public ErrorCodeMessageAttribute(string message)
        {
            this.message = message;
        }
    }
}
