﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Api
{
    public struct ErrorApiResponseWrapper
    {
        public readonly ErrorApiResponseObject error;

        public ErrorApiResponseWrapper(ErrorApiResponseObject error)
        {
            this.error = error;
        }
    }
}
