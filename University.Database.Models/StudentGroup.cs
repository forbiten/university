﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace University.Database.Models
{
    public class StudentGroup : Entity
    {
        /// <summary>
        /// Имя группы
        /// </summary>
        [Required]
        public string Name { get; set; }
        
        /// <summary>
        /// Дата организации группы.
        /// Дата с которой группа официально регистрируется учреждением.
        /// Например: 1 Сентября 2018 года.
        /// </summary>
        [Required]
        public DateTime OrganizationDate { get; set; }

        [Required]
        public string SpecialtyForeignKey { get; set; }

        /// <summary>
        /// Специальность обучения
        /// </summary>
        [ForeignKey("SpecialtyForeignKey")]
        public EducationSpecialty Specialty { get; set; }

        [Required]
        public string EducationMethodForeignKey { get; set; }

        /// <summary>
        /// Форма обучения
        /// </summary>
        [ForeignKey("EducationMethodForeignKey")]
        public EducationMethod EducationMethod { get; set; }
        
        [Required]
        public string QualificationForeignKey { get; set; }

        /// <summary>
        /// Квалификация группы
        /// </summary>
        [ForeignKey("QualificationForeignKey")]
        public Qualification Qualification { get; set; }

        /// <summary>
        /// Флаг доступности группы.
        /// True - группа была закрыта.
        /// </summary>
        [Required]
        public bool Expired { get; set; } = false;
    }
}
