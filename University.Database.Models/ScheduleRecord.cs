﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace University.Database.Models
{
    /// <summary>
    /// Модель записи расписания.
    /// Служит моделью для заполнения расписания.
    /// </summary>
    public class ScheduleRecord : Entity
    {
        public string ScheduleSheetId { get; set; }

        /// <summary>
        /// Расписание-владелец записи
        /// </summary>
        public ScheduleSheet ScheduleSheet { get; set; }

        [Required]
        public string InstructorId { get; set; }

        /// <summary>
        /// Преподаватель
        /// </summary>
        public Instructor Instructor { get; set; }

        [Required]
        public string DisciplineId { get; set; }

        /// <summary>
        /// Дисциплина
        /// </summary>
        public Discipline Discipline { get; set; }

        [Required]
        public string DisciplineMethodId { get; set; }

        /// <summary>
        /// Метод проведения занятия
        /// </summary>
        public DisciplineMethod DisciplineMethod { get; set; }

        /// <summary>
        /// Время начала занятия
        /// </summary>
        [Required]
        public int StartTime { get; set; }

        /// <summary>
        /// Продолжительность занятия
        /// </summary>
        [Required]
        public int Duration { get; set; }

        /// <summary>
        /// День проведения занятия
        /// </summary>
        [Required]
        public DayOfWeek Day { get; set; }

        /// <summary>
        /// Место проведения занятия
        /// </summary>
        [Required]
        public string Location { get; set; }
    }
}
