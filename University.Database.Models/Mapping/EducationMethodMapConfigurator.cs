﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class EducationMethodMapConfigurator : EntityMappingConfiguration<EducationMethod>
    {
        public override void Configure(EntityTypeBuilder<EducationMethod> builder)
        {
            builder
                .HasIndex(p => p.Name)
                .IsUnique(true);
        }
    }
}
