﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class ScheduleTypeMapConfigurator : EntityMappingConfiguration<ScheduleType>
    {
        public override void Configure(EntityTypeBuilder<ScheduleType> builder)
        {
            builder
                .HasIndex(p => p.Name)
                .IsUnique(true);
        }
    }
}
