﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace University.Database.Models
{
    public static class ModelBuilderMapExtensions
    {
        private static IEnumerable<Type> GetMappingTypes(this Assembly assembly, Type mappingInterface)
        {
            return assembly.GetTypes()
                .Where(x => !x.IsAbstract && x.GetInterfaces()
                .Any(y => y.GetTypeInfo().IsGenericType && y.GetGenericTypeDefinition() == mappingInterface));
        }

        public static ModelBuilder AddEntityConfigurationsFromAssembly(this ModelBuilder modelBuilder, Assembly assembly)
        {
            var mappingTypes = assembly.GetMappingTypes(typeof(IEntityMappingConfiguration<>));
            foreach (var config in mappingTypes.Select(Activator.CreateInstance).Cast<IEntityMappingConfiguration>())
                config.Configure(modelBuilder);

            return modelBuilder;
        }

        public static ModelBuilder AddMapConfiguration<TEntity>(this ModelBuilder modelBuilder, IEntityMappingConfiguration<TEntity> configurator)
            where TEntity : class
        {
            configurator.Configure(modelBuilder);
            return modelBuilder;
        }
    }
}
