﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class DisciplineMapConfigurator : EntityMappingConfiguration<Discipline>
    {
        public override void Configure(EntityTypeBuilder<Discipline> builder)
        {
            builder
                .HasIndex(p => p.Name)
                .IsUnique(true);
        }
    }
}
