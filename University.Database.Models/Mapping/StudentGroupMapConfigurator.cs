﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class StudentGroupMapConfigurator : EntityMappingConfiguration<StudentGroup>
    {
        public override void Configure(EntityTypeBuilder<StudentGroup> builder)
        {
            builder
                /// Обозначение уникального ключа для параметров группы
                .HasIndex(p => new {
                    p.Name,
                    p.OrganizationDate,
                    p.EducationMethodForeignKey,
                    p.QualificationForeignKey,
                    p.SpecialtyForeignKey
                }).IsUnique(true);
        }
    }
}
