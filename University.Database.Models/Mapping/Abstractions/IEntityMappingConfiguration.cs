﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace University.Database.Models
{
    public interface IEntityMappingConfiguration
    {
        void Configure(ModelBuilder builder);
    }

    public interface IEntityMappingConfiguration<T> : IEntityMappingConfiguration where T : class
    {
        void Configure(EntityTypeBuilder<T> builder);
    }
}
