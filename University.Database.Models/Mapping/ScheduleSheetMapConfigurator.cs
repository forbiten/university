﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace University.Database.Models
{
    public class ScheduleSheetMapConfigurator : EntityMappingConfiguration<ScheduleSheet>
    {
        public override void Configure(EntityTypeBuilder<ScheduleSheet> builder)
        {
            builder
                .HasMany(sheet => sheet.Records)
                .WithOne(record => record.ScheduleSheet)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
