﻿using System;
using System.Collections.Generic;
using System.Text;

namespace University.Authentication.VK
{
    public class VKOptions
    {
        public string AppId { get; set; }
        public string AppSecret { get; set; }
    }
}
